import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbModalModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AllCampaignsComponent } from './components/all-campaigns/all-campaigns.component';
import { EditCampaignComponent } from './components/edit-campaign/edit-campaign.component';
import { CustomNgbDatepickerFormatter } from './helpers/CustomNgbDatePickerFormatter';
import { SearchCampaignComponent } from './components/search-campaign/search-campaign.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PageNotFoundComponent,
    AllCampaignsComponent,
    EditCampaignComponent,
    SearchCampaignComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbDatepickerModule,
    NgbModalModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{provide: NgbDateParserFormatter, useClass: CustomNgbDatepickerFormatter}],
  bootstrap: [AppComponent]
})
export class AppModule { }
