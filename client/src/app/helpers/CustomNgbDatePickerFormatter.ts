import { NgbDateParserFormatter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

/**
 * Custom date formatter for Ngb Datepicker
 */
export class CustomNgbDatepickerFormatter extends NgbDateParserFormatter {

    public parse(dateString: string): NgbDateStruct {

        let date = null;

        try {

            date = new Date(dateString);

        } catch (error) {

            date = new Date();

        } finally {

            return {
                year: date.getFullYear(),
                month: date.getMonth(),
                day: date.getDay()
            } as NgbDateStruct;

        }
    }

    public format(date: NgbDateStruct): string {

        if (!date) {
            return '';
        }

        const newDate = new Date(date.year, date.month, date.day);

        return new Intl.DateTimeFormat('en-GB').format(newDate);
    }

}