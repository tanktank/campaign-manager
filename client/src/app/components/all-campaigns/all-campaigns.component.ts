import { Component, OnInit } from '@angular/core';
import { AdServerService } from 'src/app/services/data/ad-server.service';
import { Observable } from 'rxjs';
import { Campaign } from 'src/app/models/Campaign';

@Component({
  selector: 'cm-all-campaigns',
  templateUrl: './all-campaigns.component.html',
  styleUrls: ['./all-campaigns.component.scss']
})
export class AllCampaignsComponent implements OnInit {

  private campaigns: Observable<Campaign>;

  constructor(
    private adServerService: AdServerService,
  ) { }

  ngOnInit() {
    this.campaigns = this.adServerService.getAllCampaigns();
  }

}
