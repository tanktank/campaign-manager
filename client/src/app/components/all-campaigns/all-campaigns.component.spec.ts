import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCampaignsComponent } from './all-campaigns.component';
import { EditCampaignComponent } from '../edit-campaign/edit-campaign.component';
import { AdServerService } from 'src/app/services/data/ad-server.service';
import { Observable } from 'rxjs';
import { Campaign } from 'src/app/models/Campaign';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchCampaignComponent } from '../search-campaign/search-campaign.component';
import { MockComponent, MockedComponent, MockRender } from 'ng-mocks';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { tick } from '@angular/core/src/render3';

describe('AllCampaignsComponent', () => {

  let component: AllCampaignsComponent;
  let fixture: ComponentFixture<AllCampaignsComponent>;
  let getAllCampaignsSpy: jasmine.Spy;

  let campaignRowsDe: DebugElement;

  beforeEach(async(() => {

    const adServerService = jasmine.createSpyObj('AdServerService', ['getAllCampaigns']);
    getAllCampaignsSpy = adServerService.getAllCampaigns;

    TestBed.configureTestingModule({
      declarations: [
        AllCampaignsComponent,
        MockComponent(SearchCampaignComponent),
        MockComponent(EditCampaignComponent)
      ],
      providers: [
        { provide: AdServerService, useValue: adServerService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
