import { Component, OnInit } from '@angular/core';
import { AdServerService } from 'src/app/services/data/ad-server.service';

@Component({
  selector: 'cm-search-campaign',
  templateUrl: './search-campaign.component.html',
  styleUrls: ['./search-campaign.component.scss']
})
export class SearchCampaignComponent implements OnInit {

  constructor(
    private adServerService: AdServerService
  ) { }

  ngOnInit() {
  }

  private async onSearch(campaignId: string) {
    console.log({ campaignId });

    if (campaignId) {
      const campaign = await this.adServerService.getCampaign(campaignId).toPromise();
      alert(JSON.stringify(campaign));
    }
  }
}
