import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCampaignComponent } from './search-campaign.component';
import { AdServerService } from 'src/app/services/data/ad-server.service';

describe('SearchCampaignComponent', () => {
  let component: SearchCampaignComponent;
  let fixture: ComponentFixture<SearchCampaignComponent>;
  let adServerService: AdServerService;

  beforeEach(async(() => {

    adServerService = jasmine.createSpyObj('AdServerService', ['getCampaign']);

    TestBed.configureTestingModule({
      declarations: [SearchCampaignComponent],
      providers: [{ provide: AdServerService, useValue: adServerService }]
    })
      .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
