import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { EditCampaignComponent } from './edit-campaign.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdServerService } from 'src/app/services/data/ad-server.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('EditCampaignComponent', () => {
  let component: EditCampaignComponent;
  let fixture: ComponentFixture<EditCampaignComponent>;
  let openButtonDe: DebugElement;
  let openButtonEl: HTMLElement;

  // ModalService spys
  let openSpy: jasmine.Spy;

  // AdServerService spys
  let addCampaign: jasmine.Spy;

  beforeEach(async(() => {

    const modalService = jasmine.createSpyObj('NgbModal', ['open']);
    openSpy = modalService.open;

    const adServerService = jasmine.createSpyObj('AdServerService', ['addCampaign']);
    addCampaign = adServerService.addCampaign;

    TestBed.configureTestingModule({
      imports: [
        NgbDatepickerModule,
        NgbModalModule,
        ReactiveFormsModule
      ],
      declarations: [
        EditCampaignComponent
      ],
      providers: [
        { provide: NgbModal, useValue: modalService },
        { provide: AdServerService, useValue: adServerService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCampaignComponent);
    component = fixture.componentInstance;

    openButtonDe = fixture.debugElement.query(By.css('#cmOpenModalButton'));
    openButtonEl = openButtonDe.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal when the open button clicked', () => {

    openSpy.and.returnValue({result: 'close'});
    openButtonEl.click();

    expect(openSpy).toHaveBeenCalled();
  });
});
