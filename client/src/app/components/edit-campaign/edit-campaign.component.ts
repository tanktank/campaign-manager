import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AdServerService } from 'src/app/services/data/ad-server.service';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'cm-edit-campaign',
  templateUrl: './edit-campaign.component.html',
  styleUrls: ['./edit-campaign.component.scss']
})
export class EditCampaignComponent implements OnInit {

  private readonly campaign = {};
  private editCampaignForm: FormGroup;

  constructor(
    private modalService: NgbModal,
    private adServerService: AdServerService
  ) { }

  ngOnInit() {
    this.setDefaultValues(this.campaign);
    this.initialseForm();
  }

  /**
   * Selected dates on load for NgbDatepicker
   * @param campaign 
   */
  private setDefaultValues(campaign) {
    const now = new Date();

    campaign.startDate = {
      year: now.getFullYear(),
      month: now.getMonth(),
      day: now.getDate()
    } as NgbDateStruct;

    now.setDate(now.getDate() + 1);

    campaign.endDate = {
      year: now.getFullYear(),
      month: now.getMonth(),
      day: now.getDate()
    } as NgbDateStruct;

    campaign.targetImpressions = 1;
  }

  private initialseForm() {

    this.editCampaignForm = new FormGroup({
      'startDate': new FormControl(
        this.campaign['startDate'],
        Validators.required
      ),
      'endDate': new FormControl(
        this.campaign['endDate'],
        Validators.required
      ),
      'targetImpressions': new FormControl(
        this.campaign['targetImpressions'],
        Validators.required
      )
    },
      {
        // Add custom cross-field validator for checking start date and end date
        validators: EditCampaignComponent.endDateAfterStartDateValidator
      });

  }

  /**
   * Custom ValidatorFn to ensure the endDate is after startDate
   */
  private static readonly endDateAfterStartDateValidator: ValidatorFn = (formGroup: FormGroup) => {

    const startNgbDate = formGroup.get('startDate').value as NgbDateStruct;
    const endNgbDate = formGroup.get('endDate').value as NgbDateStruct;

    // Do nothing if one of the field is empty
    if (!startNgbDate || !endNgbDate) {
      return null;
    }

    const startDate = new Date(startNgbDate.year, startNgbDate.month, startNgbDate.day);
    const endDate = new Date(endNgbDate.year, endNgbDate.month, endNgbDate.day);

    return startDate.getTime() >= endDate.getTime() ? { endDateAfterStartDate: true } : null;
  }

  /**
   * Open the edit modal
   * @param content template element contains the edit form
   */
  public async open(content) {

    try {

      const command = await this.modalService
        .open(content, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static' })
        .result;

      console.log(`Closed`, command);

      switch (command) {
        case 'save':
          this.addCampaign(this.campaign);
          break;
        case 'close':
          // Do nothing;
          break;
        default:
          throw new Error(`Unexpected command: ${command}`);
      }

    } catch (closeReason) {
      console.log({ closeReason });
    }

  }

  private async addCampaign(campaign: any) {

    if (!campaign) {
      return;
    }

    try {

      const saveResult = await this.adServerService.addCampaign(campaign).toPromise();
      console.log({ saveResult });

      if (saveResult.error) {
        alert(`Error occurred when creating new campaign.\n${JSON.stringify(saveResult)}`);
      } else {
        alert(`New campaign has been created.\nCampaign ID: ${saveResult.data.addCampaign}`);
      }

    } catch (err) {

      console.error('Fail to save campaign.', campaign);
      alert(`Error occurred when creating new campaign.\n${JSON.stringify(err)}`);

    }

  }

}
