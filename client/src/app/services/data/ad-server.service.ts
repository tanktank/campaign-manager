import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Campaign } from 'src/app/models/Campaign';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class AdServerService {

  /**
   * TODO:
   * Replace this with an environment variable
   */
  private baseUrl: string = 'http://localhost:3000/';

  constructor(private httpClient: HttpClient) { }

  public getAllCampaigns(): Observable<Campaign> {

    const graphyQuery = {
      query: `
        query FetchCampaigns {
          getAllCampaigns {
            id
            startDate
            endDate
            targetImpressions
            deliveredImpressions
          }
        }
      `
    };

    return this.httpClient.post(`${this.baseUrl}graphql`, graphyQuery)
      .pipe(
        map(graphyQueryResponse => {
          // Unwrap the data
          const campaigns = graphyQueryResponse['data']['getAllCampaigns'];
          return campaigns.map(Campaign.toObject);
        })
      );
  }

  public getCampaign(campaignId: string): Observable<any> {

    const graphqlQuery = {
      query: `
        query GetCampaignById($id: ID) {
          getCampaign(campaignId: $id) {
            id
            startDate
            endDate
            targetImpressions
            deliveredImpressions
          }
        }
      `,
      variables: {
        id: campaignId
      }
    };

    return this.httpClient.post(`${this.baseUrl}graphql`, graphqlQuery)
      .pipe(
        // Unwrap the data
        map(graphqlQueryResponse => graphqlQueryResponse['data']['getCampaign'])
      );
  }

  public addCampaign(campaign): Observable<any> {

    const startDate = this.toTimestamp(campaign.startDate);
    const endDate = this.toTimestamp(campaign.endDate);
    const targetImpressions = parseInt(campaign.targetImpressions);

    const graphyQuery = {
      query: `
        mutation CreateNewCampaign($startDate: Date, $endDate: Date, $targetImpressions: Int) {
          addCampaign(campaignInput: {startDate: $startDate, endDate: $endDate, targetImpressions: $targetImpressions})
        }
      `,
      variables: {
        startDate,
        endDate,
        targetImpressions
      }
    }

    console.log('adding campaign:', { campaign });

    return this.httpClient.post(`${this.baseUrl}graphql`, graphyQuery);
  }

  /**
   * Convert NgbDateStruct { year, month, day }
   * to timestamp
   * @param date 
   */
  private toTimestamp(date: NgbDateStruct): number {

    const newDate = new Date(date.year, date.month, date.day);
    return newDate.getTime();
  }
}
