import { TestBed } from '@angular/core/testing';

import { AdServerService } from './ad-server.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AdServerService', () => {

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        AdServerService
      ]
    });

  });

  it('should be created', () => {
    const service: AdServerService = TestBed.get(AdServerService);
    expect(service).toBeTruthy();
  });
});
