export class Campaign {
    id: string;
    startDate: Number;
    endDate: Number;
    targetImpressions: Number;
    deliveredImpressions: Number;

    constructor(
        id?: string,
        startDate?: Number,
        endDate?: Number,
        targetImpressions?: Number,
        deliveredImpressions?: Number
    ) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.targetImpressions = targetImpressions;
        this.deliveredImpressions = deliveredImpressions;
    }

    public static toObject(campaignData: any): Campaign {
        return new Campaign(
            campaignData.id,
            campaignData.startDate,
            campaignData.endDate,
            campaignData.targetImpressions,
            campaignData.deliveredImpressions
        );
    }
}