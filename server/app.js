const express = require('express');
const bodyParser = require('body-parser');

const graphqlHttp = require('express-graphql');
const graphqlResolver = require('./graphql/resolvers');
const graphqlSchema = require('./graphql/schema');

const app = express();
const port = 3000;

/**
 * TODO:
 * 1. Use cors library to whitelist the client URLs
 * 2. User 'jwt' to encode the payload
 */
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});

app.use(
    '/graphql',
    graphqlHttp({
        schema: graphqlSchema,
        rootValue: graphqlResolver,
        graphiql: true,
        customFormatErrorFn(err) {

            if (!err.originalError) {
                return err;
            }

            /**
             * TODO:
             * Not sending error message back to client in Live environment
             */
            const data = err.originalError.data;
            const message = err.message || 'An error occurred.';
            const code = err.originalError.code || 500;
            return { message: message, status: code, data: data };
        }
    })
);

// to support JSON-encoded bodies
app.use(bodyParser.json());
// to support URL-encoded bodies
app.use(bodyParser.urlencoded({
    extended: false
}));

/**
 * Todo:
 * Log the error message to elsewhere
 * instead of response to client with error messages.
 */
app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data });
  });

const server = app.listen(port, () => {
    console.log("Listening on port " + server.address().port + "...");
});

module.exports = app;