const chai = require('chai');
const expect = require('chai').expect;
const nodeApp = require("../app");

chai.use(require('chai-http'));

describe("Graphql - Integration", () => {

    let chaiHttpAgent;

    beforeAll(() => {
        chaiHttpAgent = chai.request(nodeApp).keepOpen();
    });

    afterAll(() => {
        chaiHttpAgent.close();
    });

    describe(".getAllCampaigns", () => {

        const fetchCampaigns = {
            query: `
              query FetchCampaigns {
                getAllCampaigns {
                  id
                  startDate
                  endDate
                  targetImpressions
                  deliveredImpressions
                }
              }
            `
        };

        it("should return status code 200", (done) => {

            return chaiHttpAgent
                .post('/graphql')
                .send(fetchCampaigns)
                .end(function (err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it("should return array of campaign objects with expected properties", (done) => {

            return chaiHttpAgent
                .post('/graphql')
                .send(fetchCampaigns)
                .end(function (err, res) {

                    expect(err).to.be.null;

                    /**
                     * Expected response object:
                     * {
                     *  data: {
                     *      getAllCampaigns: []
                     *  }
                     * }
                     */
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    expect(res.body.data).to.be.an('object');
                    expect(res.body.data.getAllCampaigns).to.be.an('array');

                    const campaigns = res.body.data.getAllCampaigns;
                    const firstCampaign = campaigns[0];

                    /**
                     * Expected campagin object
                     * {
                     *  id: string,
                     *  startDate: number,
                     *  endDate: number,
                     *  targetImpressions: number,
                     *  deliveredImpressions: number
                     * }
                     */
                    expect(firstCampaign).to.have.keys([
                        'id',
                        'startDate',
                        'endDate',
                        'targetImpressions',
                        'deliveredImpressions'
                    ]);

                    expect(firstCampaign.id).to.be.an('string');
                    expect(firstCampaign.startDate).to.be.an('number');

                    done();
                });
        });

    });
});

xdescribe('Graphql - Isolated', () => {

    xdescribe(".getAllCampaigns", () => {});
});