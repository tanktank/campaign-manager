const { GraphQLScalarType } = require('graphql');
const { Kind } = require('graphql/language');
const adserverController = require('../controllers/adserver-controller');

module.exports = {
    // Custom Scalar Type Date
    Date: new GraphQLScalarType({
        name: 'Date',
        description: 'Date custom scalar type',
        parseValue(value) {
            // parse value from the client
            return new Date(value);
        },
        serialize(value) {
            // serialize value sent to the client
            return value.getTime();
        },
        parseLiteral(ast) {
            if (ast.kind === Kind.INT) {
                // ast value is always in string format
                return new Date(ast.value)
            }
            return null;
        },
    }),
    getAllCampaigns: adserverController.getAllCampaigns,
    getCampaign: (graphqlQuery) => {
        // Extract the query parameter(s) from requests
        const campaignId = graphqlQuery.campaignId;
        return adserverController.getCampaign(campaignId);
    },
    addCampaign: (graphqlQuery) => {
        // Extract the query parameter(s) from requests
        const newCampaign = graphqlQuery.campaignInput
        return adserverController.addCampaign(newCampaign);
    }
};