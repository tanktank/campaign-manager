const { buildSchema } = require('graphql');

module.exports = buildSchema(`

  scalar Date

  type Campaign {
    id: String
    startDate: Date
    endDate: Date
    targetImpressions: Int
    deliveredImpressions: Int
  }

  input CampaignInputDate {
    startDate: Date
    endDate: Date
    targetImpressions: Int
  }

  type RootQuery {
    getAllCampaigns: [Campaign]!
    getCampaign(campaignId: ID): Campaign
  }

  type RootMutation {
    addCampaign(campaignInput: CampaignInputDate): String
  }

  schema {
    query: RootQuery
    mutation: RootMutation
  }
`);