const axios = require('axios');
const querystring = require('querystring');
const timestampUuid = require('uuid/v1');

/**
 * This class contains menthods(all return promises) for
 * fetching data from remote server
 */

/**
 * TODO:
 * Replace this with environment variables
 */
const baseUrl = 'https://esobbc6302.execute-api.eu-west-1.amazonaws.com/default/';
const defaultConfig = {
    headers: {
        'Content-Type': 'application/json',
        'X-API-Key': 'NvHSw1vFdv22I4k9wrNBJ8U1V72ubwJAtdXNxcuc'
    }
};

exports.getAllCampaigns = async () => {

    const adServerResponse = await axios.get(`${baseUrl}campaigns/*`, defaultConfig);
    return adServerResponse.data;

};

exports.getCampaign = async (campaignId) => {

    if (!campaignId) {
        throw new Error('Invalid campaign ID is given');
    }

    const adServerResponse = await axios.get(`${baseUrl}campaigns/${campaignId}`, defaultConfig);

    return adServerResponse.data;
}

exports.addCampaign = async (campaign) => {

    if (!campaign ||
        typeof campaign.startDate != "number" ||
        typeof campaign.endDate != "number" ||
        typeof campaign.targetImpressions != "number"
    ) {
        throw new Error('startDate, endDate and targetImpressions of the saving campaign must be numbers.');
    }

    campaign.id = generateCampaignId();

    const addCampaignConfig = {
        headers: {
            'X-API-Key': 'NvHSw1vFdv22I4k9wrNBJ8U1V72ubwJAtdXNxcuc',
            'Content-Type': 'application/x-www-form-urlencoded',
        }
    };

    const adServerResponse = await axios.post(`${baseUrl}campaigns`, querystring.stringify(campaign), addCampaignConfig);
    return adServerResponse.data.id;
};

/**
 * Generate an UUID for campaign objects
 */
function generateCampaignId() {
    return `zTest${timestampUuid().replace(/-/g, '')}`;
}
