const sinon = require('sinon');
const axios = require('axios');

const adServerController = require('./adserver-controller');
const campaignHelper = require('../spec/helpers/campaign-helpers');

describe('AdServerController', () => {

    // Valid test data
    const validCampaignId = '5d726ac54dab6921b69addcc';
    const validStartDate = 10000;
    const validEndDate = 20000;
    const validTargetImpressions = 1;

    describe('.getAllCampaigns()', () => {

        it('should return at least one campaign', async () => {

            const campaigns = await adServerController.getAllCampaigns();

            expect(campaigns).toEqual(jasmine.any(Array));
            expect(campaigns).toBeDefined();
            expect(campaigns.length).toBeGreaterThan(1);

        });

        it('should return campaigns objects that contains all expected properties', async () => {

            const campaigns = await adServerController.getAllCampaigns();

            campaignHelper.itShouldBeAValidCampaign(campaigns[0]);
        });
    });

    describe('.getCampaign(id: string)', () => {

        it('should throw an Error, when no campaign ID is given', async () => {

            await expectAsync(adServerController.getCampaign())
                .toBeRejectedWith(new Error('Invalid campaign ID is given'));

        });

        it('should return an campaign object with a valid ID', async () => {

            const campaign = await adServerController.getCampaign(validCampaignId);

            campaignHelper.itShouldBeAValidCampaign(campaign);

        });

    });

    describe('.addCampaign(campaign: Campaign)', () => {

        let postStub;

        beforeEach(() => {
            postStub = sinon.stub(axios, 'post');
        });

        afterEach(() => {
            postStub.restore();
        });

        it('should throw error with no parameter', async () => {

            await expectAsync(adServerController.addCampaign(undefined))
                .toBeRejectedWith(new Error('startDate, endDate and targetImpressions of the saving campaign must be numbers.'));
        });

        it('should throw error with campaign contains invalid startDate', async () => {

            const invalidStartDateCampaign = {
                startDate: undefined,
                endDate: validEndDate,
                targetImpressions: validTargetImpressions
            };

            await expectAsync(adServerController.addCampaign(invalidStartDateCampaign))
                .toBeRejectedWith(new Error('startDate, endDate and targetImpressions of the saving campaign must be numbers.'));
        });

        it('should throw error with campaign contains invalid endDate', async () => {

            const invalidEndDateCampaign = {
                startDate: validStartDate,
                endDate: undefined,
                targetImpressions: validTargetImpressions
            };

            await expectAsync(adServerController.addCampaign(invalidEndDateCampaign))
                .toBeRejectedWith(new Error('startDate, endDate and targetImpressions of the saving campaign must be numbers.'));
        });

        it('should throw error with campaign contains invalid testTargetImpressions', async () => {

            const invalidTargetImpressionsCampaign = {
                startDate: validStartDate,
                endDate: validEndDate,
                targetImpressions: undefined
            };

            await expectAsync(adServerController.addCampaign(invalidTargetImpressionsCampaign))
                .toBeRejectedWith(new Error('startDate, endDate and targetImpressions of the saving campaign must be numbers.'));
        });

        it('should generate campaign ID and call axios.post with the campaign object', async () => {

            // Return valid data the will not
            // cause errors
            postStub.returns(
                Promise.resolve({
                    data: {
                        id: validCampaignId
                    }
                }));

            const validCampaign = {
                startDate: validStartDate,
                endDate: validEndDate,
                targetImpressions: validTargetImpressions
            };

            const savedCampaignId = await adServerController.addCampaign(validCampaign);

            expect(savedCampaignId).toEqual(validCampaignId);
        });
    });
});
