module.exports.itShouldBeAValidCampaign = (campaign) => {

    expect(campaign).toBeDefined();

    const { id, startDate, endDate, targetImpressions, deliveredImpressions } = campaign;

    expect(id).toBeDefined();
    expect(id).toEqual(jasmine.any(String));
    expect(id).toBeTruthy();

    expect(startDate).toBeDefined();
    expect(startDate).toEqual(jasmine.any(Number));
    expect(startDate).toBeGreaterThan(1);

    expect(endDate).toBeDefined();
    expect(endDate).toEqual(jasmine.any(Number));
    expect(endDate).toBeGreaterThan(1);

    expect(targetImpressions).toBeDefined();
    expect(targetImpressions).toEqual(jasmine.any(Number));

    expect(deliveredImpressions).toBeDefined();
    expect(deliveredImpressions).toEqual(jasmine.any(Number));
};